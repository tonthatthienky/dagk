$(document).ready(function(){
    $("#btnSubmit").click(function () {
        var divItem = $("<div></div>");
        divItem.attr("id", "item");
        var username = $("<h3></h3>").text($("#name").val() + " :");
        var textComment = $("<textarea></textarea>").text($("#comment").val());
        textComment.attr("disabled", "disabled");
        divItem.append(username, textComment);
        divItem.hide();

        $("#membercomment").prepend(divItem);
        divItem.show("slow");
    });
    $("#membercomment textarea").attr("disabled","disabled");
});