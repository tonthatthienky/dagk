﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _1012206.Models
{
    public class UserModel
    {
        public int id { get; set; }
        [Display(Name = "Tên Khách Hàng"), Required(ErrorMessage = "Chưa nhập tên")]
        public string u_user { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [Display(Name = "Mật khẩu")]
        [StringLength(20, ErrorMessage = "{0} cần từ {2} tới {1} ký tự! Vui lòng nhập lại!", MinimumLength = 6)]
        public string u_pass { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu xác nhận")]
        [Display(Name = "Nhập lại Mật khẩu")]
        [System.ComponentModel.DataAnnotations.Compare("u_pass", ErrorMessage = "Mật khẩu xác nhận sai! Vui lòng nhập lại")]
        public string u_repass { get; set; }

        [Display(Name = "Email Liên Hệ"), Required(ErrorMessage = "Chưa nhập Email")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Vui lòng điền đúng định dạng email")]
        public string u_email { get; set; }

        [Display(Name = "Giới tính")]
        public string u_sex { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn ngày sinh")]
        [Display(Name = "Ngày tháng năm sinh")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string u_bday { get; set; }
        [Display(Name = "Nơi ở hiện tại")]
        public string u_location { get; set; }

        [Display(Name = "Số Điện Thoại"), Required(ErrorMessage = "Chưa nhập số điện thoại")]
        [StringLength(11, ErrorMessage = "{0} cần từ {2} tới {1} ký tự! Vui lòng nhập lại!", MinimumLength = 6)]
        public string u_phone { get; set; }
    }

    public class LoginModel
    {

        [Required(ErrorMessage = "tai khoan khong duoc trong")]//chek tai khoan khac null
        [RegularExpression("[a-zA-Z0-9]{4,20}", ErrorMessage = "Tai khoan khong duoc chua ky tu dac biet")]
        //chek tai khoan khong duoc chua ky tu dac biet va nam trong khoang 4->20 ky tu
        public String u_user { get; set; }

        [Required(ErrorMessage = "mat khau khong duoc trong")]
        [RegularExpression("[a-zA-Z0-9]{4,20}", ErrorMessage = "mat khau khong duoc chua ky tu dac biet")]
        public String u_pass { get; set; }
    }
    public class Search
    {
        [RegularExpression("[a-zA-Z0-9]{1,10}")]
        public String Searchs { get; set; }
    }
}
