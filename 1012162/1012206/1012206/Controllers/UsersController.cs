﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using PagedList;

using _1012206.Models;

namespace _1012206.Controllers
{
    public class UsersController : Controller
    {
        UsersDBEntities db = new UsersDBEntities();
  
        public ActionResult Index()
        {
            
                return View(db.users.ToList());
        }
        public ActionResult Search(string a)
        {
            var task = from t in db.users select t;
            if(!string.IsNullOrEmpty(a))
            {
                task = task.Where(x => x.u_user.Contains(a));
            }
            return View(task);
        }
     
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Users/Create

        [HttpPost]
        public ActionResult Create(user user)
        {
            if (ModelState.IsValid)
            {
                db.users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(user);
        }

        //
        // GET: /Users/Edit/5

        public ActionResult Edit(string id = null)
        {
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Edit/5

        [HttpPost]
        public ActionResult Edit(user user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Users");
            }
            return View(user);
        }

        //
        // GET: /Users/Delete/5

        public ActionResult Delete(string id = null)
        {
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            user user = db.users.Find(id);
            db.users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        //
        // GET: /Login/
        public ActionResult Login()
        {
            LoginModel lg = new LoginModel();
            return View("Login", lg);
        }
        [HttpPost]
        public ActionResult Login(LoginModel lg)//nhan lg khi an login o from login
        {
            if (ModelState.IsValid)//chek tai khoan va mat khau dung cu phap
            {
                //thoa man dieu kien
                UsersDBEntities db = new UsersDBEntities();
                var data = (from d in db.users
                            where d.u_user.Equals(lg.u_user) && d.u_pass.Equals(lg.u_pass)
                            select d).ToList();//lay du lieu voi dk tk va mk 
                if (data.Count > 0)
                {
                    //neu tai khoan ton tai ta gan cho session
                    Session["Login"] = lg.u_user;
                    //goi den ham GetRolesForUser o customroleprovider
                    FormsAuthentication.SetAuthCookie(lg.u_user, false);
                    return RedirectToAction("Index", "Users");
                }
                ViewBag.mess = "Sai Tai Khoan Hoac Mat Khau";
                return View("Login", lg);
            }
            return View("Login", lg);//neu khong dung cu phap thi quay lai form dang nhap bao loi
        }
        public ActionResult LogOut()
        {
            Session["Login"] = null;
            FormsAuthentication.SignOut();//gan quyen = null
            LoginModel lg = new LoginModel();
            return View("Login", lg);//goi den trang Login va truyen cho cai model lg of Login
        }
        public ActionResult Error()
        {
            return View("Error");
        }

    }

}