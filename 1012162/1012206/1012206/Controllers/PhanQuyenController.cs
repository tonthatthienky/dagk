﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1012206.Controllers
{
    public class PhanQuyenController : Controller
    {
        //
        // GET: /PhanQuyen/

        [Authorize(Roles = "admin")]
        public ActionResult Admin()
        {
            return View("Admin");
        }
        [Authorize(Roles = "admin,nhanvien,nguoidung")]
        public ActionResult NguoiDung()
        {
            return View("NguoiDung");
        }
    }
}
