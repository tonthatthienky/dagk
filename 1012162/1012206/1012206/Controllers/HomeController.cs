﻿using _1012206.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1012206.Controllers
{
    public class HomeController : Controller
    {

        private UsersDBEntities db = new UsersDBEntities();
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        [HttpPost]
        public ActionResult Index(UserModel umodel)
        {
            user userdb = new user();
            if (ModelState.IsValid)
            {

                userdb.u_user = umodel.u_user;
                userdb.u_pass = umodel.u_pass;
                userdb.u_email = umodel.u_email;
                userdb.u_sex = umodel.u_sex;
                userdb.u_bday = Convert.ToDateTime(umodel.u_bday);
                userdb.u_adress = umodel.u_location;
                userdb.u_phone = umodel.u_phone;

                ViewBag.u_user = umodel.u_user;
                ViewBag.u_pass = umodel.u_pass;
                ViewBag.u_email = umodel.u_email;
                ViewBag.u_sex = umodel.u_sex;
                ViewBag.u_bday = Convert.ToDateTime(umodel.u_bday);
                ViewBag.u_adress = umodel.u_location;
                ViewBag.u_phone = umodel.u_phone;

                //return View("Info");

                // return RedirectToAction("Info");
                try
                {
                    db.users.Add(userdb);
                    db.SaveChanges();
                    return View("Info");
                }
                catch
                {
                    ViewBag.ErrorMessage = "Tên đăng nhập đã tồn tại! Vui lòng chọn tên khác!";
                    return View();
                }
            }
            return View();
            // return View(userdb);
        }

        public ActionResult Info()
        {
            ViewBag.Message = "Đăng ký tài khoản thành công";

            return View();
        }
    }
}
